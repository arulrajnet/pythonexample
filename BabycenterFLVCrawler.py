__author__ = 'arul'

"""
    Baby center child birth video classes downloader
"""

def download_file(__url__):
    import urllib2
    try:
        __content__ = urllib2.urlopen(__url__).read()
    except Exception, e:
        print "Error %s for URL %s" % (e.message, __url__)
    file_name = get_name_from_url(__url__)
    # windows supports max length of filename is 193
    if len(file_name) > 190:
        file_name = file_name[0:190]
    with open(DOWNLOAD_FOLDER+'/'+file_name, "wb") as flvflile:
        if __content__:
            flvflile.write(__content__)

def is_exists(__url__):
    import urllib2
    try:
        urllib2.urlopen(__url__)
    except urllib2.HTTPError, e:
        return False
    except urllib2.URLError, e:
        return False
    return True

def get_name_from_url(__url__):
    from urlparse import urlparse
    parsed_url = urlparse(__url__)
    name = parsed_url.path.rsplit('/')[len(parsed_url.path.rsplit('/')) - 1]
    return name

if __name__ == "__main__":
    DOWNLOAD_FOLDER = "flvs"
    url_list = ["http://netst.babycenter.com/video/labor/class_labor_narration_slide%s_1.flv" % '%02d' % slide
                for slide in xrange(01, 89)]

    download_list = list()

    for url in url_list:
        if is_exists(url):
            download_list.append(url)

    print "URL List %d" % len(url_list)
    print "Downloadable URL %d " % len(download_list)

    print "Error URL %r " % (set(url_list) - set(download_list))

    import os
    if not os.path.exists(DOWNLOAD_FOLDER):
        os.makedirs(DOWNLOAD_FOLDER)

    for download in download_list:
        download_file(download)
