__author__ = 'arul'

from time import sleep
import sys

for i in xrange(1, 101):
    sys.stdout.write('\r')
    # the exact output you're looking for:
    sys.stdout.write("[%-100s] %d%%" % ('='*i, i))
    sys.stdout.flush()
    sleep(0.05)
