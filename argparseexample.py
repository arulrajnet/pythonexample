#!/usr/bin/python
import argparse
__author__ = 'Arul'

class SomethingArg(object):
  """docstring for SomethingArg"""
  def __init__(self, arg):
    super(SomethingArg, self).__init__()
    self.arg = arg
    
  def main(self):
    parser = argparse.ArgumentParser(description='This is a script for url to image by PiQube')
    parser.add_argument('-u','--url', help='URL of the page',required=True)
    parser.add_argument('-o','--output',help='Output file name', required=True)
    parser.add_argument('-W','--width',help='Width of the output image', type=int, required=False)
    parser.add_argument('-H','--height',help='Height of the output image', type=int, required=False)
    args = parser.parse_args()
     
    ## show values ##
    print ("URL : %s" % args.url )
    print ("Output file: %s" % args.output )
    if args.width and args.height:
      print ("Width: %s" % args.width )
    print ("Height: %s" % args.height )

  if __name__ == '__main__':
    #somethingArg = new SomethingArg();
    main()