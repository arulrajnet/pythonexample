import urllib
import urlparse

url = "http://graph.facebook.com/4"
fields = ['id','name_format','name','first_name','last_name','middle_name','username','gender','link','picture','cover','education','work','bio','about','birthday','age_range','address','email','website','location','hometown','verified','locale','timezone','political','religion','relationship_status','languages','interested_in','third_party_id','updated_time','is_verified','currency','devices','install_type','installed','favorite_athletes','favorite_teams','inspirational_people','meeting_for','payment_pricepoints','significant_other','sports','quotes','test_group','security_settings','video_upload_limits','viewer_can_send_gift']
FB_PAGE_FIELDS = ['id','about','affiliation','app_id','artists_we_like','attire','awards','band_interests','band_members','best_page','bio','birthday','booking_agent','built','business','can_post','category','category_list','company_overview','cover','culinary_team','current_location','description','description_html','directed_by','emails','features','food_styles','founded','general_info','general_manager','genre','global_brand_page_name','global_brand_parent_page','has_added_app','hometown','hours','influences','is_community_page','is_permanently_closed','is_published','is_unclaimed','is_verified','link','location','mission','mpg','name','network','new_like_count','offer_eligible','parent_page','parking','payment_options','personal_info','personal_interests','pharma_safety_info','phone','plot_outline','press_contact','price_range','produced_by','promotion_eligible','promotion_ineligible_reason','public_transit','record_label','release_date','restaurant_services','restaurant_specialties','schedule','screenplay_by','season','starring','store_number','studio','talking_about_count','total_likes_sentence','unread_message_count','unread_notif_count','unseen_message_count','username','website','were_here_count','written_by','checkins','likes','members','products']
params = {'lang':'en','tag':'python', 'fields': fields}

url_parts = list(urlparse.urlparse(url))
print url_parts
query = dict(urlparse.parse_qsl(url_parts[4]))
print query
print url_parts[4]
query.update(params)
print query
url_parts[4] = urllib.urlencode(query)
#print urllib.urldecode(query)
print url_parts[4]
print urlparse.urlunparse(url_parts)

params = urllib.urlencode(params)

if urlparse.urlparse(url)[4]:
    print url + '&' + params
else:
    print url + '?' + params