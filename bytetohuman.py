# -*- coding: utf-8 -*-

__author__ = 'arul'

"""
"""

import sys

def byte_to_human(len_in_byte):
    print len_in_byte
    in_kb = len_in_byte / 1024
    in_mb = in_kb / 1024
    in_gb = in_mb / 1024

    print in_kb, "%.2f KB" % in_kb
    print in_mb, "%.2f MB" % in_mb
    print in_gb, "%.2f GB" % in_gb

    if in_kb < 1024:
        return "\r%2d KB" % in_kb

    if in_mb < 1024:
        return "\r%2d MB" % in_mb

    if in_gb > 1:
        return "\r%2d GB" % in_gb

if __name__ == '__main__':
    # for x in xrange(0, 1024*1024*5):
    #     if x % 10 == 0:
    #         sys.stdout.write(byte_to_human(x))
    #         sys.stdout.flush()

    print byte_to_human(1024*1024*5.2)
