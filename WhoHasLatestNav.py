import requests, sys
from lxml import html
from bs4 import BeautifulSoup

vrs_value = "NA"
amfi_value = "NA"
rss_value = "NA"
mstar_value = "NA"


def get_content(url, is_html=True):
    try:
        u_response = requests.get(url)
        u_content = u_response.text

        if is_html:
            soup = BeautifulSoup(u_response.text)
            u_content = soup.prettify()
        return u_content

    except:
        return None


if __name__ == "__main__":
    """
    Program starts here
    """

    # print "Test"
    print """"%s","%s","%s","%s","%s" """ % ('Date', 'VRSO', 'MSTAR', 'AMFI', 'RSS')

    while True:
        # Read from valueresearchonline.com
        vrs_content = get_content('https://www.valueresearchonline.com/funds/newsnapshot.asp?schemecode=15714')

        if vrs_content:
            tree = html.document_fromstring(vrs_content)
            vrs_direct_growth = tree.xpath("//div[contains(@class,'fund-type')]/ul/li[1]/text()")

            vrs_value = vrs_direct_growth[1].strip()

        # Read from morningstar.in
        mstar_content = get_content('http://www.morningstar.in/mutualfunds/f00000pdl7/uti-equity-fund-growth-option-direct/risk-ratings.aspx')

        if mstar_content:
            tree = html.document_fromstring(mstar_content)
            mstar_direct_growth = tree.xpath("//*[@id='ctl00_ContentPlaceHolder1_ucQuoteHeader_new_component']/div[1]/div[1]/span/span[2]/text()")
            mstar_value = mstar_direct_growth[0].strip()

        # Read from AMFI NAV0.txt
        amfi_content = get_content('http://www.portal.amfiindia.com/spages/NAV0.txt', False)

        if amfi_content:
            import re
            match = re.search("120662(.*)", amfi_content)
            if match:
                matched_value = match.group()
                matched_splits = matched_value.split(';')
                amfi_value = matched_splits[4]

        # Read from AMFI RSS data
        # rss_content = get_content('http://portal.amfiindia.com/RssNAV.aspx?mf=28', False)
        #
        # if rss_content:
        #     y = BeautifulSoup(rss_content, 'xml')
        #
        #     items = y.find_all('item')
        #     for item in items:
        #         print item.find("description").text

        from datetime import datetime

        print """"%s","%s","%s","%s","%s" """ % (datetime.now(), vrs_value, mstar_value, amfi_value, rss_value)
        sys.stdout.flush()
        import time
        time.sleep(60)