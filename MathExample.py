__author__ = 'arul'

import math

"""
note :
    float("-inf"), float("+inf"), float("nan")
    are python representations

    test with math.isinf(), math.isnan()
"""

def scale(r) :
    if math.isnan(r)  :
        return None
    elif 0.5 <= r <= 2.0 :
        return 5.0 + 5.0*(math.log(r) / math.log(2))
    elif r > 2.0 :
        return 10.0
    elif r < 0.5 :
        return 0.0
    else :
        return 5.0


if __name__ == '__main__':
    print scale(1)
    print scale(0.9)
    print scale(1.4)
    print scale(5)
    print scale(3)
    print scale(0.2)
    print scale(0.6)