__author__ = 'arul'

from pymongo import MongoClient

import re
import sys

hostname = "localhost"
dbname = "piqube-crawler-db"
collectionname = "linkedin_profiles_normalized"

__client = None
__db = None
collections = {}

def connect():
    global __client, __db, collections
    try:
        __client = MongoClient(hostname, 27017)
    except:
        raise ValueError("I am unable to connect to the database. Host %s, Database %s" % (hostname, dbname))

    __db = __client[dbname]

    for cname in __db.collection_names(include_system_collections=False):
        collections[cname] = __db[cname]


if __name__ == '__main__':
    connect()

    query = {}
    projection = None  # {"_id": 1}
    documents = list()
    try:
        __cursor = collections[collectionname].find(query, projection)
    except:
        raise ValueError("Error on execute query %s" % query)

    __cursor.batch_size(1000)

    norm_black_list = set(['crawledDate', 'version', 'profileViews'])
    final_norm_list = list()
    final_raw_list = list()
    for doc in __cursor:
        raw_data = dict(doc["raw_data"])
        normalized_data = dict(doc["normalized_data"])
        raw_keys = set(raw_data.keys())
        norm_keys = set(normalized_data.keys())
        norm_diff = set(norm_keys - raw_keys) - norm_black_list
        if len(norm_diff) > 0:
            # print doc["_id"]
            # print norm_diff
            final_norm_list.extend(norm_diff)
        raw_diff = raw_keys - norm_keys
        if len(raw_diff) > 0:
            if "honors" in raw_diff:
                print doc["_id"]
                print raw_diff
            final_raw_list.extend(raw_diff)

    print "Final Norm : %s" % set(final_norm_list)
    print "Final Raw : %s" % set(final_raw_list)