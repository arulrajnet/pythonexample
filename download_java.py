#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
    from urllib.request import Request, urlopen
except ImportError:  # python 2
    from urllib2 import Request, urlopen
import re
import os

valid_packages = ['jre', 'server-jre', 'jdk']
valid_java_versions = xrange(7, 9, 1)


def regex_websearch(url, pattern):
    req = Request(url)
    resp = urlopen(req)
    content = resp.read().decode('UTF-8')
    resp.close()
    match = re.search(pattern, content)
    return match


def download_binary(_file_name_, _req_):
    print "Downloading %s to %s " % (_req_.get_full_url(), _file_name_)
    resp = urlopen(_req_)
    CHUNK = 16 * 1024
    with open(_file_name_, 'wb') as fp:
        while True:
            chunk = resp.read(CHUNK)
            if not chunk:
                break
            fp.write(chunk)


def download_latest_java(java_version=8, package='server-jre', extension='tar.gz',
                         architecture='linux-x64'):
    if package not in valid_packages:
        print('Invalid Java package selection, valid packages are:')
        for valid_package in valid_packages:
            print('\t' + valid_package)
        return None

    url = "http://www.oracle.com"

    url_1 = url + "/technetwork/java/javase/downloads/index.html"
    pattern_1 = '\/technetwork\/java/\javase\/downloads\/' + package + str(java_version) + '-downloads-.+?\.html'
    match = regex_websearch(url_1, pattern_1)

    if match is None:
        print('Unable to download Java from ' + url_1)
        print('Website is down or script is outdated')
        return None

    # Finding tar.gz or rpm endpoint
    url_2 = url + match.group(0)
    pattern_2 = "http\:\/\/download.oracle\.com\/otn-pub\/java\/jdk\/[7-9]u[0-9]+?-.+?\/" + package + "-[7-9]u[0-9]+?-" + architecture + "." + extension
    match = regex_websearch(url_2, pattern_2)

    if match is None:
        print('Selected architecture.extension \"' + architecture + '.' + extension + '\" is not available')
        print('Visit \"' + url_2 + '\" to see available architectures and extensions')
        return None

    # Add cookie to request and download the file to current directory
    req = Request(match.group(0))
    req.add_header('Cookie', 'gpw_e24=http://www.oracle.com/;oraclelicense=accept-securebackup-cookie')
    file_name = os.path.basename(match.group(0))

    download_binary(file_name, req)


if __name__ == '__main__':
    selected_package = "jdk"
    selected_version = 8

    for index, item in enumerate(valid_packages):
        if item != selected_package:
            print "%s.%s" % (index + 1, item)
        else:
            print "%s.%s (Default)" % (index + 1, item)

    given_package = raw_input("Please select the package : ")

    if given_package in valid_packages:
        selected_package = given_package

    for index, item in enumerate(valid_java_versions):
        if item != selected_version:
            print "%s" % item
        else:
            print "%s (Default)" % item

    given_version = raw_input("Please select the version : ")

    try:
        if int(given_version) in valid_java_versions:
            selected_version = given_version
    except Exception as e:
        pass

    download_latest_java(selected_version, selected_package)