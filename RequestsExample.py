import requests

class RequestExample(object):
  """docstring for RequestExample"""
  def __init__(self, arg):
    super(RequestExample, self).__init__()
    self.arg = arg

url = "http://rapportive.com/login_status?user_email=java@gmail.com"
reponse = requests.get(url)
print reponse.text
print "reponse headers : \n%s" % (reponse.headers)
print "request headers : \n%s" % (reponse.request.headers)