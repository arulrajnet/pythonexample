# This example for listing a number from defined range
start = 0
end = 400
pages = []

while start <= end:
	pages.append(start)
	start+=1
	#pass

for page in pages:
	print("Page number is %r" % (page));

start_urls = [
    "https://kickass.to/movies/%d/" % page for page in pages
]

print start_urls