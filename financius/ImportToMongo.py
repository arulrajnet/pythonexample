__author__ = 'arulraj'

from pymongo import MongoClient

import re
import sys
import json
import io
import uuid
from pprint import pprint

hostname = "localhost"
dbname = "financius"

collection_names_v4 = ["currencies_v4", "categories_v4", "accounts_v4", "transactions_v4"]
collection_names_v9 = ["currencies_v9", "categories_v9", "accounts_v9", "transactions_v9", "tags_v9"]

__client = None
__db = None
collections = {}

def connect():
    global __client, __db, collections
    try:
        __client = MongoClient(hostname, 27017)
    except:
        raise ValueError("I am unable to connect to the database. Host %s, Database %s" % (hostname, dbname))

    __db = __client[dbname]

    for cname in __db.collection_names(include_system_collections=False):
        collections[cname] = __db[cname]


def dropAndReloadFromJSON():

    for cname in collection_names_v4:
        __db.drop_collection(cname)
        __db.create_collection(cname)

    with io.open('/home/arulraj/Downloads/backup.json', encoding='UTF-8') as json_file:
        json_data=json.loads(json_file.read())

    transactions = json_data["transactions"]
    accounts = json_data["accounts"]
    currencies = json_data["currencies"]
    categories = json_data["categories"]

    currency_collection = collections["currencies_v4"]
    for currency in currencies:
        currency["_id"] = currency["id"]
        currency_collection.save(currency)


    categories_collection = collections["categories_v4"]
    for category in categories:
        category["_id"] = category["id"]
        categories_collection.save(category)


    accounts_collection = collections["accounts_v4"]
    for account in accounts:
        account["_id"] = account["id"]
        accounts_collection.save(account)


    transactions_collection = collections["transactions_v4"]
    for transaction in transactions:
        transaction["_id"] = transaction["id"]
        transactions_collection.save(transaction)

def loadV9Currencies():

    currency_collection = collections["currencies_v4"]
    currency_collection_v9 = collections["currencies_v9"]

    __cursor = currency_collection.find({})

    for doc in __cursor:
        curV9 = dict()
        curV9["code"] = doc["code"]
        curV9["symbol"] = doc["symbol"]
        curV9["decimal_separator"] = doc["decimal_separator"]
        curV9["group_separator"] = doc["group_separator"]
        curV9["decimal_count"] = doc["decimals"]
        curV9["symbol_position"] = 2
        curV9["sync_state"] = 1
        curV9["model_state"] = 1
        curV9["id"] = str(uuid.uuid4())
        curV9["_id"] = curV9["id"]
        currency_collection_v9.save(curV9)

def loadV9Accounts():

    global main_account_id, investment_account_id

    account_collection = collections["accounts_v4"]
    account_collection_v9 = collections["accounts_v9"]

    __cursor = account_collection.find({ 'balance': { '$gt': 0 } } )

    for doc in __cursor:
        accV9 = dict()
        accV9["currency_code"] = "INR"
        accV9["title"] = "My Bank"
        accV9["note"] = "Combined balance of all bank accounts and hot cash"
        accV9["balance"] = int(doc["balance"] * 100)
        accV9["include_in_totals"] = True
        accV9["sync_state"] = 1
        accV9["model_state"] = 1
        accV9["id"] = str(uuid.uuid4())
        accV9["_id"] = accV9["id"]
        main_account_id = accV9["id"]
        account_collection_v9.save(accV9)

    accV9 = dict()
    accV9["currency_code"] = "INR"
    accV9["title"] = "My Investment"
    accV9["note"] = "All kind of my investment"
    accV9["balance"] = 0
    accV9["include_in_totals"] = True
    accV9["sync_state"] = 1
    accV9["model_state"] = 1
    accV9["id"] = str(uuid.uuid4())
    accV9["_id"] = accV9["id"]
    investment_account_id = accV9["id"]
    account_collection_v9.save(accV9)

    accV9 = dict()
    accV9["currency_code"] = "INR"
    accV9["title"] = "My Lend"
    accV9["note"] = "All kind of kadan given to friends and family"
    accV9["balance"] = 0
    accV9["include_in_totals"] = True
    accV9["sync_state"] = 1
    accV9["model_state"] = 1
    accV9["id"] = str(uuid.uuid4())
    accV9["_id"] = accV9["id"]
    account_collection_v9.save(accV9)

    accV9 = dict()
    accV9["currency_code"] = "INR"
    accV9["title"] = "My Loan"
    accV9["note"] = "Money borrowed from friends and family"
    accV9["balance"] = 0
    accV9["include_in_totals"] = True
    accV9["sync_state"] = 1
    accV9["model_state"] = 1
    accV9["id"] = str(uuid.uuid4())
    accV9["_id"] = accV9["id"]
    account_collection_v9.save(accV9)


def loadV9Categories():

    __cursor = collections['categories_v4'].find({'id':{'$in':[4,9,17,24,29,34,40,50,56,59] + [55]}})

    i = 0
    for doc in __cursor:
        catV9 = dict()

        catV9["old_id"] = doc["id"]
        catV9["title"] = doc["title"]
        catV9["color"] = doc["color"]
        old_tr_type = doc["type"]
        if old_tr_type == 0:
            catV9["transaction_type"] = 2
        elif old_tr_type == 1:
            catV9["transaction_type"] = 1
        elif old_tr_type == 2:
            catV9["transaction_type"] = 3

        i += 1
        catV9["sort_order"] = i

        catV9["sync_state"] = 1
        catV9["model_state"] = 1
        catV9["id"] = str(uuid.uuid4())
        catV9["_id"] = catV9["id"]

        collections['categories_v9'].save(catV9)


def loadV9Tags():

    cat_as_tag = set()
    __cursor = collections['transactions_v4'].distinct("category_id")

    old_cat_ids = list()
    for doc in __cursor:
        old_cat_ids.append(int(doc))

    cat_to_remove = [4,9,17,24,29,34,40,50,56,59] + [0,1,2,3] + [55, 51, 70, 78, 19]

    cat_as_tag = set(old_cat_ids) - set(cat_to_remove)
    __cursor = collections['categories_v4'].find({'id':{'$in': list(cat_as_tag)}})

    for doc in __cursor:
        tagv9 = dict()

        tagv9["old_id"] = doc["id"]
        tagv9["title"] = doc["title"]
        tagv9["sync_state"] = 1
        tagv9["model_state"] = 1
        tagv9["id"] = str(uuid.uuid4())
        tagv9["_id"] = tagv9["id"]

        collections['tags_v9'].save(tagv9)

    for t_name in ["Ration", "Bus Tickets", "Freelance / Contract", "Sales", "Kitchen Utilities", "Party", "Tailoring",
                   "Beauty", "ULIP", "Endowment"]:
        tagv9 = dict()
        tagv9["title"] = t_name
        tagv9["sync_state"] = 1
        tagv9["model_state"] = 1
        tagv9["id"] = str(uuid.uuid4())
        tagv9["_id"] = tagv9["id"]

        collections['tags_v9'].save(tagv9)



def loadV9Transactions():

    __cursor = collections['transactions_v4'].find({'delete_state':0})

    for doc in __cursor:
        transV9 = dict()

        if doc["state"] == 0:
            transV9["transaction_state"] = 1

        # expense
        if doc["account_from_id"] == 3 and doc["account_to_id"] == 2:
            transV9["transaction_type"] = 1
            transV9["account_from_id"] = main_account_id
            transV9["account_to_id"] = None
        # income
        elif doc["account_from_id"] == 1 and doc["account_to_id"] == 3:
            transV9["transaction_type"] = 2
            transV9["account_from_id"] = None
            transV9["account_to_id"] = main_account_id

        cc = collections["categories_v9"].find({"old_id": doc["category_id"]})

        for cat in cc:
            transV9["category_id"] = cat["id"]

        tt = collections["tags_v9"].find({"old_id": doc["category_id"]})

        tags = list()
        for tag in tt:
            tags.append(tag["id"])

            kk = collections["categories_v4"].find({"id":doc["category_id"]})
            for t_cat in kk:
                t_parent = t_cat["parent_id"]

                pcat = collections["categories_v9"].find({"old_id":t_parent})

                for ppcat in pcat:
                    transV9["category_id"] = ppcat["id"]

        # its saving
        if doc["category_id"] in [70,74,75,76,77,78]:
            transV9["transaction_type"] = 3
            transV9["account_from_id"] = main_account_id
            transV9["account_to_id"] = investment_account_id
            transV9["category_id"] = None

            if doc["note"].lower().__contains__("chit"):
                tr = collections["tags_v9"].find({"title": "Chit"})
                for tag in tr:
                    tags.append(tag["id"])

            if doc["note"].lower().__contains__("ppf"):
                tr = collections["tags_v9"].find({"title": "Ppf"})
                for tag in tr:
                    tags.append(tag["id"])

        # child care
        if doc["category_id"] in [51]:

            note = doc["note"]

            tr = collections["categories_v9"].find({"title":"Health and Beauty"})
            hb_cat = None
            for docc in tr:
                hb_cat = docc["id"]

            tr = collections["categories_v9"].find({"title":"Household"})
            hh_cat = None
            for docc in tr:
                hh_cat = docc["id"]

            kk = collections["tags_v9"].find({"title":"Accessories"})
            ac_tag = None
            for docc in kk:
                ac_tag = docc["id"]

            kk = collections["tags_v9"].find({"title":"Stationary"})
            ss_tag = None
            for docc in kk:
                ss_tag = docc["id"]

            kk = collections["tags_v9"].find({"title":"Cosmetics"})
            cc_tag = None
            for docc in kk:
                cc_tag = docc["id"]

            if note.lower().__contains__("bottle"):
                transV9["category_id"] = hh_cat
                tags.append(ac_tag)
            elif note.lower().__contains__("toys"):
                transV9["category_id"] = hh_cat
                tags.append(ss_tag)
            elif note.lower().__contains__("lotion"):
                transV9["category_id"] = hb_cat
                tags.append(cc_tag)
            else:
                transV9["category_id"] = hh_cat
                tags.append(ac_tag)

        # shoes
        if doc["category_id"] in [19]:
            kk = collections["categories_v9"].find({"title":"Clothing"})
            for docc in kk:
                transV9["category_id"] = docc["id"]

            kk = collections["tags_v9"].find({"title":"Accessories"})
            for docc in kk:
                tags.append(docc["id"])

        transV9["tag_ids"] = tags

        transV9["date"] = doc["date"]
        # To work as decimal point
        transV9["amount"] = int(doc["amount"] * 100)
        transV9["note"] = doc["note"]
        transV9["include_in_reports"] = True
        transV9["exchange_rate"] = 1
        transV9["sync_state"] = 1
        transV9["model_state"] = 1
        transV9["id"] = doc["server_id"]
        transV9["_id"] = transV9["id"]

        collections["transactions_v9"].save(transV9)


def exportAsJSON():

    finalJson = dict()

    finalJson["version"] = 9
    import time
    finalJson["timestamp"] = int(time.time()) * 1000

    transactions = list()
    accounts = list()
    currencies = list()
    categories = list()
    tags = list()

    c_transactions_v9 = collections["transactions_v9"]
    for doc in c_transactions_v9.find({}):
        del doc["_id"]
        transactions.append(doc)

    c_accounts_v9 = collections["accounts_v9"]
    for doc in c_accounts_v9.find({}):
        del doc["_id"]
        accounts.append(doc)

    c_currencies_v9 = collections["currencies_v9"]
    for doc in c_currencies_v9.find({}):
        del doc["_id"]
        currencies.append(doc)

    c_categories_v9 = collections["categories_v9"]
    for doc in c_categories_v9.find({}):
        del doc["_id"]
        del doc["old_id"]
        categories.append(doc)

    c_tags_v9 = collections["tags_v9"]
    for doc in c_tags_v9.find({}):
        del doc["_id"]
        if "old_id" in doc:
            del doc["old_id"]
        tags.append(doc)

    finalJson["currencies"] = currencies
    finalJson["accounts"] = accounts
    finalJson["categories"] = categories
    finalJson["tags"] = tags
    finalJson["transactions"] = transactions

    with io.open('/home/arulraj/Downloads/final-json.json', 'w', encoding="UTF-8") as json_file:
        json_file.write(unicode(json.dumps(finalJson, indent = 2, ensure_ascii=False)))


if __name__ == '__main__':
    connect()
    # dropAndReloadFromJSON()

    for cname in collection_names_v9:
        __db.drop_collection(cname)
        __db.create_collection(cname)

    loadV9Currencies()
    loadV9Accounts()
    loadV9Categories()
    loadV9Tags()
    loadV9Transactions()
    exportAsJSON()
