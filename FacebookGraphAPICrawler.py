import os

class Settings(object):

  # Mongo DB server details
  MONGODB_SERVER = 'localhost'
  MONGODB_PORT = 27017
  MONGODB_DB = 'piqube-crawler-db'
  MONGODB_COLLECTION = 'fb_graph_api_profiles'
  MONGODB_UNIQ_KEY = '_id'

  LOG_FILE_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "FacebookGraphAPICrawler.log")

  FB_START_ID = 4
  FB_END_ID = 100
  # Fields are open in User node
  FB_PROFILE_FIELDS = ['id','name_format','name','first_name','last_name','middle_name','username','gender','link','picture','cover','education','work','bio','about','birthday','age_range','address','email','website','location','hometown','verified','locale','timezone','political','religion','relationship_status','languages','interested_in','third_party_id','updated_time','is_verified','currency','devices','install_type','installed','favorite_athletes','favorite_teams','inspirational_people','meeting_for','payment_pricepoints','significant_other','sports','quotes','test_group','security_settings','video_upload_limits','viewer_can_send_gift']

# Fields are open in Page node
FB_PAGE_FIELDS = ['id','about','affiliation','app_id','artists_we_like','attire','awards','band_interests','band_members','best_page','bio','birthday','booking_agent','built','business','can_post','category','category_list','company_overview','cover','culinary_team','current_location','description','description_html','directed_by','emails','features','food_styles','founded','general_info','general_manager','genre','global_brand_page_name','global_brand_parent_page','has_added_app','hometown','hours','influences','is_community_page','is_permanently_closed','is_published','is_unclaimed','is_verified','link','location','mission','mpg','name','network','new_like_count','offer_eligible','parent_page','parking','payment_options','personal_info','personal_interests','pharma_safety_info','phone','plot_outline','press_contact','price_range','produced_by','promotion_eligible','promotion_ineligible_reason','public_transit','record_label','release_date','restaurant_services','restaurant_specialties','schedule','screenplay_by','season','starring','store_number','studio','talking_about_count','total_likes_sentence','unread_message_count','unread_notif_count','unseen_message_count','username','website','were_here_count','written_by','checkins','likes','members','products']

class APIResponse(object):
  code = None
  message = None
  data = None

  def __repr__(self):
    return "APIResponse {code: %s, message: %s, data: %s}" % (self.code, self.message, self.data)

from os import path
import csv

class LogWriter(object):
  """docstring for LogWriter"""

  def __init__(self):
    self.create_path_if_not_exist(Settings.LOG_FILE_PATH)    
    if not path.exists(Settings.LOG_FILE_PATH):
      logfile = open(Settings.LOG_FILE_PATH, "wb")
      self.csv_writer = csv.writer(logfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
      csv_header = ['Id','HTTP code','Message','Data']
      self.csv_writer.writerow(csv_header)
    else:
      logfile = open(Settings.LOG_FILE_PATH, 'a')
      self.csv_writer = csv.writer(logfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)


  def create_path_if_not_exist(self, filePath):
    if not path.exists(path.dirname(filePath)):
      os.makedirs(path.dirname(filePath))

  def write_log(self, message):
    if(self.csv_writer):
        self.csv_writer.writerow(message)


from urllib2 import Request, urlopen, URLError, HTTPError
import urllib
import urlparse
import time

class FacebookGraphAPICrawler(object):

  def __init__(self):
    pass

  def do_api_request(self, url):
    apiresponse = APIResponse()
    params = {'fields':Settings.FB_PROFILE_FIELDS}
    params = urllib.urlencode(params)

    if urlparse.urlparse(url)[4]:
        apiurl = url + '&' + params
    else:
        apiurl = url + '?' + params    
    try:
      response = urlopen(apiurl)
      if (response is not None):
        apiresponse.data = response.read()
        apiresponse.code = response.getcode()
        apiresponse.message = "Success"
    except HTTPError as e:
      apiresponse.code = e.code
      apiresponse.data = e.read()
      apiresponse.message = "Failed"
    except URLError as e:
      if hasattr(e, 'reason'): 
        print "Failed to reach the server..."
        apiresponse.code = 000
        apiresponse.data = "%s Failed to reach the server %s..." % (e.reason, url)
        apiresponse.message = "Failed"
      else:
          #Everything else
          raise

    return apiresponse

class MongoDBPipeline(object):

  def __init__(self):
    from pymongo import MongoClient
    client = MongoClient(Settings.MONGODB_SERVER, Settings.MONGODB_PORT)
    self.db = client[Settings.MONGODB_DB]
    self.collection = self.db[Settings.MONGODB_COLLECTION]
    if self.__get_uniq_key() is not None:
        self.collection.create_index(self.__get_uniq_key(), unique=True)

  def process_item(self, item):
    if self.__get_uniq_key() is None:
        self.collection.insert(dict(item))
    else:
        self.collection.update(
                        {self.__get_uniq_key(): item['id']},
                        dict(item),
                        upsert=True)  
    print("Item wrote to MongoDB database %s/%s" %
                (Settings.MONGODB_DB, Settings.MONGODB_COLLECTION))  
    return item

  def __get_uniq_key(self):
    if not Settings.MONGODB_UNIQ_KEY or Settings.MONGODB_UNIQ_KEY == "":
        return None
    return Settings.MONGODB_UNIQ_KEY

#
# Code starting here
#

import json

for fb_unique_id in xrange(Settings.FB_START_ID, Settings.FB_END_ID):
  api_url = "http://graph.facebook.com/%s" % (fb_unique_id)
  print "Check for FB User %s " % (fb_unique_id)
  time.sleep(0.25)
  crawler = FacebookGraphAPICrawler()
  logger = LogWriter()
  db = MongoDBPipeline()
  apiresponse = crawler.do_api_request(api_url)
  print "Response code %s " % (apiresponse.code)
  if apiresponse.code == 200:
    d = json.loads(apiresponse.data)
    d['_id'] = d['id']
    d['_api_status_message'] = apiresponse.message
    db.process_item(d);
    logger.write_log([fb_unique_id, apiresponse.code, apiresponse.message, ''])
  elif apiresponse.code == 400:
    #Bad request error
    print "Not found..."
    logger.write_log([fb_unique_id, apiresponse.code, apiresponse.message, apiresponse.data])
  elif apiresponse.code == 404:
    #Have to recheck for this id
    print "File Not found..."
    logger.write_log([fb_unique_id, apiresponse.code, apiresponse.message, apiresponse.data])
  else:
    print "apiresponse.code : %s " % (apiresponse.code)
    logger.write_log([fb_unique_id, apiresponse.code, apiresponse.message, apiresponse.data])