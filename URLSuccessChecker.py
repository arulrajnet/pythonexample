import httplib
import urllib2

urls = [
    "http://m.ak.fbcdn.net/profile.ak/1939620_10101266232851011_437577509_%s.jpg" % (letter)
    for letter in ("a")
    # for letter in ("abcdefghijklmnopqrstuvwxyz")
]

for url in urls:
    try:
        response = urllib2.urlopen(url)
        # print 'response headers: "%s"' % response.info()
        if response.getcode() == 200:
            print 'http error code: 200'
            print url
    except IOError, e:
        if hasattr(e, 'code'):  # HTTPError
            print 'http error code: ', e.code
        elif hasattr(e, 'reason'):  # URLError
            print "can't connect, reason: ", e.reason
        else:
            raise

graph_apis = [
    "http://graph.facebook.com/%s?fields=id,picture,name,first_name,last_name,middle_name,username,gender,link,locale" % (
    number)
    for number in xrange(4, 100)
]

for api in graph_apis:
    try:
        response = urllib2.urlopen(api)
        # print 'response headers: "%s"' % response.info()
        if response.getcode() == 200:
            print 'http error code: 200'
            print api
    except IOError, e:
        if hasattr(e, 'code'):  # HTTPError
            # print 'http error code: ', e.code
            print
        elif hasattr(e, 'reason'):  # URLError
            # print "can't connect, reason: ", e.reason
            print
        else:
            raise