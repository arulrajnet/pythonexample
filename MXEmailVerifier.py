# -*- coding: utf-8 -*-

"""
Verify the given mail id is valid or not without sending mail. Verifying the email id directly from mail exchange server.
"""

import argparse
import smtplib
import dns.resolver
import re
import logging

__author__ = 'arul'


class MXEmailVerifier():
    FROM_ADDRESSES = ["info@gmail.com", "contact@gmail.com", "contact@yahoo.com", "info@yahoo.com",
                      "support@google.com"]

    def __init__(self):
        self.init_logging()

    def init_logging(self):
        """
        Initialize the logs
        """
        global logger
        logger = logging.getLogger('EmailVerifier')
        logger.setLevel(logging.DEBUG)
        # create console handler with a higher log level
        console = logging.StreamHandler()
        console.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        console.setFormatter(formatter)
        # add the handlers to logger
        logger.addHandler(console)

    def __parse__(self, _email_):
        """
        Parse and find the email id from the string using regex.
        :param _email_:
        :return:
        """
        p_email = re.findall(r"\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b", _email_, re.I)
        if p_email and len(p_email) > 0:
            return p_email[0]
        else:
            raise ValueError("Not a valid email id %s." % _email_)
        return None

    def __find_mx_host__(self, _email_):
        """
        It returns the mail exchange servers sorted by preference for the given mail id.
        :param _email_:
        :return:
        """
        _host_ = _email_.split("@")[1]
        answers = dns.resolver.query(_host_, 'MX')
        # Sorting based on the preference
        answers = sorted(answers, key=lambda answer: answer.preference)
        hosts = list()
        for rdata in answers:
            hosts.append(rdata.exchange.to_text())
        if hosts and len(hosts) > 0:
            return hosts
        else:
            raise ValueError("MX server for host '%s' not able to find." % _host_)
        return None

    def verify(self, _email_):
        """
        Verify the given email id is present in their mail exchange server.
        :param _email_:
        :return:
        """
        # parsing given input as email id
        parsed_email_id = self.__parse__(_email_)
        logger.info("Parsed Email id : %s" % parsed_email_id)

        # Finding mail exchange server of the parsed mail id
        _mx_hosts_ = self.__find_mx_host__(parsed_email_id)

        for _mx_host_ in _mx_hosts_:
            _mx_host_ = _mx_host_[:-1] if _mx_host_[-1] == "." else _mx_host_
            logger.debug("MX host is %s " % _mx_host_)

            # Login into the mail exchange server
            try:
                smtp = smtplib.SMTP()
                smtp.connect(_mx_host_)
            except smtplib.SMTPConnectError, e:
                logger.error(e)
                continue

            try:
                smtp.ehlo_or_helo_if_needed()
            except Exception, e:
                logger.error(e)
                continue

            # First Try to verify with VRFY
            # 250 is success code. 400 or greater is error.
            v_code, v_message = smtp.verify(_email_)
            logger.debug("VERIFY %s, %r" % (v_code, v_message))
            if v_code and v_code != 250:
                for _from_ in self.FROM_ADDRESSES:
                    f_code, f_message = smtp.mail(_from_)
                    logger.debug("FROM %s, %r" % (f_code, f_message))
                    # Then use RCPT to verify
                    if f_code and f_code == 250:
                        r_code, r_message = smtp.rcpt(_email_)
                        logger.debug("RCPT %s, %r" % (r_code, r_message))
                        if r_code and r_code == 250:
                            return True, r_message
                        if r_code and r_code == 550:
                            return False, r_message
                    else:
                        continue
            else:
                return True, v_message
            smtp.quit()
            break
        return None, None


class EmailItem():
    def __init__(self, _domain_name_, _first_name_, _middle_name_="", _last_name_=""):
        self.domain_name = _domain_name_
        self.first_name = _first_name_
        self.middle_name = _middle_name_
        self.last_name = _last_name_

    def __repr__(self):
        pass

    def __str__(self):
        pass


class EmailPatterns():
    PATTERNS = ["{fn}", "{ln}", "{fn}{ln}", "{fn}.{ln}", "{fi}{ln}", "{fi}.{ln}", "{fn}{li}", "{fn}.{li}", "{fi}{li}",
                "{fi}.{li}", "{ln}{fn}", "{ln}.{fn}", "{ln}{fi}", "{ln}.{fi}", "{li}{fn}", "{li}.{fn}", "{li}{fi}",
                "{li}.{fi}", "{fi}{mi}{ln}", "{fi}{mi}.{ln}", "{fn}{mi}{ln}", "{fn}.{mi}.{ln}", "{fn}{mn}{ln}",
                "{fn}.{mn}.{ln}", "{fn}-{ln}", "{fi}-{ln}", "{fn}-{li}", "{fi}-{li}", "{ln}-{fn}", "{ln}-{fi}",
                "{li}-{fn}", "{li}-{fi}", "{fi}{mi}-{ln}", "{fn}-{mi}-{ln}", "{fn}-{mn}-{ln}", "{fn}_{ln}",
                "{fi}_{ln}", "{fn}_{li}", "{fi}_{li}", "{ln}_{fn}", "{ln}_{fi}", "{li}_{fn}", "{li}_{fi}",
                "{fi}{mi}_{ln}", "{fn}_{mi}_{ln}", "{fn}_{mn}_{ln}"]

    def __init__(self):
        pass

    def find_patterns(self, _email_item_):
        pass
        fn = _email_item_.first_name
        fi = fn[0]

        ln = li = mn = mi = ""
        if _email_item_.last_name and len(_email_item_.last_name.strip()) > 0:
            ln = _email_item_.last_name
            li = ln[0]

        if _email_item_.middle_name and len(_email_item_.middle_name.strip()) > 0:
            mn = _email_item_.middle_name
            mi = mn[0]

        emails = list()
        for p in self.PATTERNS:
            emails.append(p.format(fn=fn, mn=mn, ln=ln, fi=fi, mi=mi, li=li)+"@"+_email_item_.domain_name)

        return emails

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Verify the given mail id is valid or not.")
    parser.add_argument("-f", "--fname", help='Enter the First Name', required=True)
    parser.add_argument("-l", "--lname", help='Enter the Last Name', required=False, default="")
    parser.add_argument("-m", "--mname", help='Enter the Middle Name', required=False, default="")
    parser.add_argument("-d", "--domainname", help='Enter domain of the company. For ex: tcs.com', required=True)
    args = parser.parse_args()

    item = EmailItem(_domain_name_="commercekarma.com", _first_name_="Kiran", _last_name_="Rathod")
    item = EmailItem(_domain_name_=args.domainname, _first_name_=args.fname, _last_name_=args.lname,
                     _middle_name_=args.mname)
    email_pattern = EmailPatterns()
    verifier = MXEmailVerifier()

    emails = email_pattern.find_patterns(item)
    print emails

    for email in emails:
        try:
            verified, message = verifier.verify(email)
            logger.info("%s, %s, %r" % (email, verified, message))
        except Exception, e:
            logger.error(e)