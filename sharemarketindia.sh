#!/bin/bash
#
#
# Author : Arul <mailto:me@arulraj.net>


BASE_URL="http://finance.google.com/finance/info?client=ig&q="

stock_exchanges=(
INDEXBOM:SENSEX \
NSE:NIFTY \
INDEXBOM:BSE-100 \
INDEXBOM:BSE-200 \
INDEXBOM:BSE-500 \
INDEXBOM:BSE-SMLCAP \
INDEXBOM:BSE-MIDCAP \
NSE:NIFTYJR
)

function getJsonVal() {
    python -c "import json,sys;sys.stdout.write(json.loads(sys.stdin))";
}

function get_content() {
    local content=$(curl -k -L -s $1)
    echo "$content"
}

function build_url() {
    local q_param=""

    for i in "${stock_exchanges[@]}"
    do
        q_param="${q_param}${i},"
    done

    echo "${BASE_URL}${q_param}"
}

function get_google_finance_content() {
    local gfinance_url=$(build_url)

    local json_content=$(get_content $gfinance_url)

    local content_length=${#json_content}

    echo "${json_content:4:content_length}"
}

function main() {

    local json=$(get_google_finance_content)

    echo $json

    echo $(echo $json | getJsonVal "['id']")

}

main $*