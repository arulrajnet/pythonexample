__author__ = 'arulraj'


class Interval(object):
    def __init__(self, _numeric_, _alphabet_):
        self.alphabet = _alphabet_.upper()
        self.numeric = _numeric_

    def __str__(self):
        return ''.join([str(self.numeric), self.alphabet])

    def __repr__(self):
        return self.__str__()

    def __cmp__(self, other):
        if self.alphabet < other.alphabet:
            return -1
        elif self.alphabet > other.alphabet:
            return 1
        else:
            if self.numeric < other.numeric:
                return -1
            elif self.numeric > other.numeric:
                return 1
            else:
                return 0

if __name__ == "__main__":
    pass

    dd = Interval(1, "D")
    y = Interval(1, "Y")
    m = Interval(4, "M")
    d = Interval(5, "D")
    mm = Interval(10, "M")
    yy = Interval(8, "Y")
    ddd = Interval(250, "D")

    i_list = [y, m, d, dd, mm, yy, ddd]

    print i_list

    # i_list.sort(key=lambda x,y: x.numeric, y.alphabet)

    # i_list.sort()

    print sorted(i_list)