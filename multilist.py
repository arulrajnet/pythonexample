class myDict(dict):

    def __init__(self):
        self = dict()

    def add(self, key, value):
        self[key] = value


# This example for listing a number from defined range
start = 0
end = 3
pages = []

while start <= end:
	pages.append(start)
	start+=1
	#pass

names = ["zero","one","two","three"]

numbers = {}
number = {}
numberList = []

i = 0
while i < len(names) :
  print(names[i])
  print(pages[i])
  numberField = {}
  numberField["name"] = names[i]
  numberField["value"] = pages[i]
  numberList.append(numberField)
  page = pages[i]
  i=i+1
  #pass

number["number"] = numberList
numbers["numbers"] = number
print(numbers)

for name, value in zip(names, pages):
  print("Name %s Value %d " % (name, value))

for x, name in enumerate(names):
    print("i %d name %s " % (x,name)) 

for page in pages:
	print("Page number is %r" % (page));

start_urls = [
    "https://kickass.to/movies/%d/" % page for page in pages
]

print start_urls
