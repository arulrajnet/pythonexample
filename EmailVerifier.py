# -*- coding: utf-8 -*-

__author__ = 'arul'

"""
program to verify email address from http://verify-email.org/
"""

import requests
from bs4 import BeautifulSoup
from lxml import html
import re
import os
import time

class EmailVerify:

    global headers, cookies, hidden_value

    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) "
                      "Chrome/37.0.2062.124 Safari/537.36",
        "Referer": "http://google.com",
        "Accept-Encoding": "gzip,deflate,sdch",
        "Accept-Language": "en-US,en;q=0.8,ta;q=0.6",
        "Accept": "text/html, application/xml, text/xml, */*",
        "X-Requested-With": "XMLHttpRequest",
        "Connection": "keep-alive"
    }

    cookies = dict()

    hidden_value = None

    def __init__(self):
        pass

    def get_unique_key(self, _content):
        tree = html.document_fromstring(_content)
        hidden_inputs = tree.xpath("//*[@id='myForm']//input[@type='hidden']")
        for input in hidden_inputs:
            name = input.attrib['name']
            value = input.attrib['value']
            if len(name) == 32:
                return (name, value)
        return None

    def connect(self):
        global hidden_value, cookies
        r = requests.get("http://verify-email.org/", headers=headers)
        cookies["b38b8c1b3d6a4a2d4cc2696153b7cd63"] = r.cookies['b38b8c1b3d6a4a2d4cc2696153b7cd63']
        __content__ = r.text
        if __content__:
            soup = BeautifulSoup(__content__)
            _unique = self.get_unique_key(soup.prettify())
            hidden_value = _unique[0]

    def verify(self, email):
        url = "http://verify-email.org/index.php?check=%s" \
              "&%s=1" \
              "&option=com_emailverifier&view=emailverifier&layout=verify&format=raw" \
              "&submitEmail=Verify" % (email, hidden_value)
        _headers = headers
        _headers["Referer"] = "http://verify-email.org/"
        r = requests.get(url, headers=_headers, cookies=cookies)
        match = re.search(r"(?<=Result:)(.*)(?=</h3)", r.text)
        print url
        print r.text
        if match:
            if match.group() == "Ok":
                return True, match.group()
            else:
                return False, match.group()
        else:
            return False, "Limit exceeded"


def renew_ip():
    os.system("python /root/ip_renew/ip_renew.sh")

"""
Code starts from here
"""
if __name__ == '__main__':
    email_verify = EmailVerify()

    in_file_path = "emails.txt"

    if os.path.exists(in_file_path):
        out_file = open(os.path.basename(__file__)+".csv", "wb")
        with open(in_file_path, "rb") as txt_file:
            for line in txt_file:
                if line and len(line) > 0:
                    email_verify.connect()
                    line = line.strip()
                    verified = email_verify.verify(line)
                    out_line = '\"'
                    out_line += line
                    out_line += "\",\""
                    out_line += str(verified[0])
                    out_line += "\",\""
                    out_line += str(verified[1])
                    out_line += "\"\n"
                    out_file.write(out_line.encode("utf-8"))
                    if verified[1] == "Limit exceeded":
                        # TODO if its failed try again for that email id.
                        time.sleep(30)

    email = "arulraj1985@gmail.com"
    print "Validating %s" % email
    email_verify.connect()
    print email_verify.verify(email)